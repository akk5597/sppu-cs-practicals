class Node:
	def __init__(self, name, hval=0):
		self.name = name
		self.hval = hval
		self.neighbours = []
	
	def set_neighbours(self, neighbours):
		for neighbour in neighbours:
			self.neighbours.append(neighbour)
			
	def __repr__(self):
		return self.name
		
	def __str__(self):
		return self.name

def best_first_search(nodes, start, goal):
	start = nodes[start]
	goal = nodes[goal]
	
	open_list = [start]
	closed_list = []
	
	while goal not in closed_list:
		min_nodes = sorted(open_list, key=lambda x:x.hval)
		i = 0
		curr_node = min_nodes[i]
		while curr_node in closed_list:
			i += 1
			curr_node = min_nodes[i]
		closed_list.append(curr_node)
		open_list.extend([x for x in curr_node.neighbours if x not in closed_list])
		open_list.remove(curr_node)
		
	print('->'.join([n.name for n in closed_list]))

if __name__ == '__main__':
	nodes = {}
	
	nodes['S'] = Node('S', 0)
	nodes['A'] = Node('A', 3)
	nodes['B'] = Node('B', 6)
	nodes['C'] = Node('C', 5)
	nodes['D'] = Node('D', 9)
	nodes['E'] = Node('E', 8)
	nodes['F'] = Node('F', 12)
	nodes['G'] = Node('G', 14)
	nodes['H'] = Node('H', 7)
	nodes['I'] = Node('I', 5)
	nodes['J'] = Node('J', 6)
	nodes['K'] = Node('K', 1)
	nodes['L'] = Node('L', 10)
	nodes['M'] = Node('M', 4)
	
	nodes['S'].set_neighbours([nodes['A'], nodes['B'], nodes['C']])
	nodes['A'].set_neighbours([nodes['S'], nodes['E'], nodes['D']])
	nodes['B'].set_neighbours([nodes['S'], nodes['G'], nodes['F']])
	nodes['C'].set_neighbours([nodes['S'], nodes['H']])
	nodes['D'].set_neighbours([nodes['A']])
	nodes['E'].set_neighbours([nodes['A']])
	nodes['F'].set_neighbours([nodes['B']])
	nodes['G'].set_neighbours([nodes['B']])
	nodes['H'].set_neighbours([nodes['C'], nodes['J'], nodes['I']])
	nodes['I'].set_neighbours([nodes['H'], nodes['K'], nodes['L'], nodes['M']])
	nodes['J'].set_neighbours([nodes['H']])
	nodes['K'].set_neighbours([nodes['I']])
	nodes['L'].set_neighbours([nodes['I']])
	nodes['M'].set_neighbours([nodes['I']])
	
	best_first_search(nodes, 'S', 'I')
